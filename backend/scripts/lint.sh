#!/bin/bash -e

set -x

mypy app
black -l 120 -S app --check
isort --recursive --check-only app
flake8
