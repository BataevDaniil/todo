#!/bin/bash -e
set -x

autoflake --remove-all-unused-imports --recursive --remove-unused-variables --in-place app --exclude=__init__.py
black -l 120 -S app
isort --recursive --apply app
