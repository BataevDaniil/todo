from typing import Optional

from pydantic import BaseModel

import datetime

from todo.models.task import TaskStatus


class TaskBase(BaseModel):
    title: Optional[str] = None
    description: Optional[str] = None
    dt_created: Optional[datetime.datetime] = None
    dt_updated: Optional[datetime.datetime] = None
    cron: Optional[str] = None
    priority: Optional[int] = None
    status: Optional[TaskStatus] = None

    prev_task_id: Optional[int] = None
    group_id: Optional[int] = None
    updated_user_id: Optional[int] = None
    parent_id: Optional[int] = None


class TaskInDBBase(TaskBase):
    id: Optional[int] = None

    class Config:
        orm_mode = True


class TaskCreate(TaskBase):
    title: str
    dt_created: datetime.datetime
    dt_updated: datetime.datetime
    status: TaskStatus
    group_id: int
    created_user_id: int
    updated_user_id: int


class TaskUpdate(TaskBase):
    """Update task schema"""


class Task(TaskInDBBase):
    """Main task schema"""
