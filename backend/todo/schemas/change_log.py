from typing import Optional

import datetime

from pydantic import BaseModel


class ChangeLogBase(BaseModel):
    dt: Optional[datetime.datetime] = None
    data: Optional[str] = None

    user_id: Optional[int] = None
    group_id: Optional[int] = None


class ChangeLogInDBBase(ChangeLogBase):
    id: Optional[int] = None

    class Config:
        orm_mode = True


class ChangeLogCreate(ChangeLogBase):
    dt: datetime.datetime
    data: str

    user_id: int
    group_id: int


class ChangeLogUpdate(ChangeLogBase):
    """Main update change log schema"""


class ChangeLog(ChangeLogInDBBase):
    """Main change log schema"""
