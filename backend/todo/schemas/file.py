from typing import Optional

import datetime

from pydantic import BaseModel


class FileBase(BaseModel):
    name: Optional[str] = None
    path: Optional[str] = None

    comment_id: Optional[int] = None
    task_id: Optional[int] = None


class FileInDBBase(FileBase):
    id: Optional[int] = None

    class Config:
        orm_mode = True


class FileCreate(FileBase):
    name: str
    path: str
    dt_created: datetime.datetime

    comment_id: int
    task_id: int


class FileUpdate(FileBase):
    """Main update file schema"""


class File(FileInDBBase):
    """Main file schema"""
