from typing import Optional

from pydantic import BaseModel

import datetime


class NotificationBase(BaseModel):
    dt_created: Optional[datetime.datetime] = None
    dt_deleted: Optional[datetime.datetime] = None

    task_id: Optional[int] = None


class NotificationInDBBase(NotificationBase):
    id: Optional[int] = None

    class Config:
        orm_mode = True


class NotificationCreate(NotificationBase):
    dt_created: datetime.datetime
    task_id: int


class NotificationUpdate(NotificationBase):
    """Update notification schema"""


class Notification(NotificationInDBBase):
    """Main notification schema"""
