from typing import Optional

import datetime

from pydantic import BaseModel


class CommentBase(BaseModel):
    text: Optional[str] = None
    dt_updated: Optional[datetime.datetime] = None

    updated_user_id: Optional[int] = None


class CommentInDBBase(CommentBase):
    id: Optional[int] = None

    class Config:
        orm_mode = True


class CommentCreate(CommentBase):
    text: str
    dt_created: datetime.datetime
    created_user_id: int
    task_id: int


class CommentUpdate(CommentBase):
    """Main update comment schema"""


class Comment(CommentInDBBase):
    """Main comment schema"""
