from typing import Optional

import datetime

from pydantic import BaseModel


class GroupBase(BaseModel):
    name: Optional[str] = None
    color: Optional[str] = None
    dt_created: Optional[datetime.datetime] = None
    dt_updated: Optional[datetime.datetime] = None
    dt_deleted: Optional[datetime.datetime] = None
    prev_group_id: Optional[int] = None


class GroupInDBBase(GroupBase):
    id: Optional[int] = None

    class Config:
        orm_mode = True


class GroupCreate(GroupBase):
    name: str
    dt_created: datetime.datetime
    dt_updated: datetime.datetime
    created_user_id: int


class GroupUpdate(GroupBase):
    """Main update group schema"""


class Group(GroupInDBBase):
    """Main group schema"""
