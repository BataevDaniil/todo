from sqlalchemy import Boolean, Column, Integer, String
from sqlalchemy.orm import relationship

from .group import user_group_association_table
from todo.db.base_class import Base


class User(Base):
    id = Column(Integer, primary_key=True, unique=True, index=True)
    email = Column(String, unique=True, index=True, nullable=False)
    hashed_password = Column(String, nullable=False)
    is_active = Column(Boolean(), default=True)
    is_superuser = Column(Boolean(), default=False)
    last_name = Column(String(length=256), nullable=False)
    first_name = Column(String(length=256), nullable=False)

    change_logs = relationship('ChangeLog', back_populates='user')
    avatar_id = relationship('File', back_populates='user')
    created_comments = relationship('Comment', back_populates='created_user')
    updated_comments = relationship('Comment', back_populates='updated_user')
    created_groups = relationship('Comment', back_populates='updated_user')

    groups = relationship('Group', secondary=user_group_association_table, back_populates='users')
