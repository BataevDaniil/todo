from sqlalchemy import Column, DateTime, ForeignKey, Integer, Text
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from todo.db.base_class import Base


class Comment(Base):
    id = Column(Integer, primary_key=True)
    text = Column(Text, nullable=True)
    dt_created = Column(DateTime, default=func.now(), nullable=False)
    dt_updated = Column(DateTime, default=func.now(), nullable=False)

    task_id = Column(Integer, ForeignKey('task.id'), nullable=False)
    created_user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    updated_user_id = Column(Integer, ForeignKey('user.id'), nullable=False)

    files = relationship('File', back_populates='comment')
    task = relationship('Task', back_populates='comments')
    created_user = relationship('User', back_populates='created_comments', foreign_keys=[created_user_id])
    updated_user = relationship('User', back_populates='updated_comments', foreign_keys=[updated_user_id])

