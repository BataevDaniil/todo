from sqlalchemy import Column, DateTime, Integer, String, Text, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from todo.db.base_class import Base


class File(Base):
    id = Column(Integer, primary_key=True)
    name = Column(String(length=256), nullable=False)
    path = Column(Text, nullable=False)
    dt_created = Column(DateTime, default=func.now(), nullable=False)

    comment_id = Column(Integer, ForeignKey('user.id'))
    task_id = Column(Integer, ForeignKey('task.id'))

    comment = relationship('Comment', back_populates='files')
    task = relationship('Task', back_populates='files')
