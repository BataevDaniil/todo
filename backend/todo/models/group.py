from sqlalchemy import Column, DateTime, ForeignKey, Integer, String, Table
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from todo.db.base_class import Base

user_group_association_table = Table('users_groups', Base.metadata,
    Column('user_id', Integer, ForeignKey('user.id')),
    Column('group_id', Integer, ForeignKey('group.id'))
)


class Group(Base):
    id = Column(Integer, primary_key=True)
    name = Column(String(length=256), nullable=False)
    color = Column(String(length=256))
    dt_created = Column(DateTime, default=func.now(), nullable=False)
    dt_updated = Column(DateTime, default=func.now(), nullable=False)
    dt_deleted = Column(DateTime, nullable=True)

    created_user_id = Column(Integer, ForeignKey('user.id'))
    prev_group_id = Column(Integer, ForeignKey('group.id'))

    change_logs = relationship('ChangeLog', back_populates='group')
    updated_user = relationship('User', back_populates='created_groups')

    users = relationship('User', secondary=user_group_association_table, back_populates="groups")
