from enum import Enum as pyEnum

from sqlalchemy import Column, DateTime, Enum, ForeignKey, Integer, String, Text
from sqlalchemy.sql import func

from sqlalchemy.orm import relationship

from todo.db.base_class import Base


class TaskStatus(pyEnum):
    PROGRESS = 'progress'
    DONE = 'done'
    DELETED = 'deleted'


class Task(Base):
    id = Column(Integer, primary_key=True)
    title = Column(String(length=256), nullable=False)
    description = Column(Text, nullable=True)
    dt_created = Column(DateTime, default=func.now(), nullable=False)
    dt_updated = Column(DateTime, default=func.now(), nullable=False)
    cron = Column(String(length=100), nullable=True)
    priority = Column(Integer, nullable=True)
    status = Column(Enum(TaskStatus), nullable=True)

    prev_task_id = Column(Integer, ForeignKey('task.id'))
    group_id = Column(Integer, ForeignKey('group.id'))
    created_user_id = Column(Integer, ForeignKey('user.id'))
    updated_user_id = Column(Integer, ForeignKey('user.id'))
    parent_id = Column(ForeignKey('task.id'))

    files = relationship('File', back_populates='task')
    comments = relationship('Comment', back_populates='task')
    notifications = relationship('Notification', back_populates='task')
