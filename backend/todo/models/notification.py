from sqlalchemy import Column, DateTime, ForeignKey, Integer
from sqlalchemy.orm import relationship

from todo.db.base_class import Base


class Notification(Base):
    id = Column(Integer, primary_key=True)
    dt_created = Column(DateTime, nullable=True)
    dt_deleted = Column(DateTime, nullable=True)

    task_id = Column(Integer, ForeignKey('task.id'))

    task = relationship('Task', back_populates='notifications')
