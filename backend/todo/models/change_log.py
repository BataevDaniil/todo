from sqlalchemy import Column, DateTime, ForeignKey, Integer, Text
from sqlalchemy.orm import relationship

from todo.db.base_class import Base


class ChangeLog(Base):
    id = Column(Integer, primary_key=True)
    dt = Column(DateTime, nullable=True)
    data = Column(Text, nullable=True)

    user_id = Column(Integer, ForeignKey('user.id'))
    group_id = Column(Integer, ForeignKey('group.id'))

    user = relationship('User', back_populates='change_logs')
    group = relationship('Group', back_populates="change_logs")

