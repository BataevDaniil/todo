from todo.crud.base import CRUDBase
from todo.models.task import Task
from todo.schemas.task import TaskCreate, TaskUpdate


class CRUDTask(CRUDBase[Task, TaskCreate, TaskUpdate]):
    """Task's CRUD"""


task = CRUDTask(Task)
