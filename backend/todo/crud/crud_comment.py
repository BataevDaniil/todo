from todo.crud.base import CRUDBase
from todo.models.comment import Comment
from todo.schemas.comment import CommentCreate, CommentUpdate


class CRUDComment(CRUDBase[Comment, CommentCreate, CommentUpdate]):
    """Comment's CRUD"""


comment = CRUDComment(Comment)
