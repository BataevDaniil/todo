from typing import Union, Dict, Any

from sqlalchemy.orm import Session

from todo.crud.base import CRUDBase
from todo.models.change_log import ChangeLog
from todo.schemas.change_log import ChangeLogCreate, ChangeLogUpdate


class CRUDChangeLog(CRUDBase[ChangeLog, ChangeLogCreate, ChangeLogUpdate]):
    """ChangeLog's CRUD"""

    def update(self, db: Session, *, db_obj: ChangeLog, obj_in: Union[ChangeLogUpdate, Dict[str, Any]]) -> ChangeLog:
        raise Exception('Updating is prohibited')


change_log = CRUDChangeLog(ChangeLog)
