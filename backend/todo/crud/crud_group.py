from todo.crud.base import CRUDBase
from todo.models.group import Group
from todo.schemas.group import GroupCreate, GroupUpdate


class CRUDGroup(CRUDBase[Group, GroupCreate, GroupUpdate]):
    """Group's CRUD"""


group = CRUDGroup(Group)
