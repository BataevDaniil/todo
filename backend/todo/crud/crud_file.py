from todo.crud.base import CRUDBase
from todo.models.file import File
from todo.schemas.file import FileCreate, FileUpdate


class CRUDFile(CRUDBase[File, FileCreate, FileUpdate]):
    """File's CRUD"""


file = CRUDFile(File)
