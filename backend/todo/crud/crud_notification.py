from todo.crud.base import CRUDBase
from todo.models.notification import Notification
from todo.schemas.notification import NotificationCreate, NotificationUpdate


class CRUDNotification(CRUDBase[Notification, NotificationCreate, NotificationUpdate]):
    """Notification's CRUD"""


notification = CRUDNotification(Notification)
