# Import all the models, so that Base has them before being
# imported by Alembic
from todo.models import ChangeLog, Comment, File, Group, Notification, Task, User  # noqa
from todo.db.base_class import Base  # noqa