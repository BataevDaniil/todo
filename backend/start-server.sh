#!/usr/bin/env bash

cd ./todo/ && uvicorn --env-file ../.env --root-path ./todo/ main:app
